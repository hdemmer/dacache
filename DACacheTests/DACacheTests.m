//
//  DACacheTests.m
//  DACacheTests
//
//  Created by Hendrik Demmer on 1/29/13.
//  Copyright (c) 2013 Hendrik Demmer. All rights reserved.
//

#import "DACacheTests.h"

#import "DACache.h"

@implementation DACacheTests


- (void)testCacheBasicFunctionality
{
    // cache basic functionality
    // can it keep a few objects in memory?
    // will it return the correct objects?
    // will it empty

    // NSCache will not guarantee any capacity, so this test may fail for it
    NSArray * cacheClassNames = @[@"DACacheMutableDictionary", @"DACachePagedLRU"];
    
    for (NSString * cacheClassName in cacheClassNames)
    {
        for (NSUInteger capacity = 1; capacity<=10000; capacity*=10)
        {
            DACache * cache = [[NSClassFromString(cacheClassName) alloc] initWithCapacity:capacity];
            
            for (NSInteger i = 0; i<capacity; i++)
            {
                NSData * key = [[NSString stringWithFormat:@"%d", i] dataUsingEncoding:NSUTF8StringEncoding];
                
                [cache setObject:[NSString stringWithFormat:@"test%d", i] forKey:key];
            }
            
            for (NSInteger i = 0; i<capacity; i++)
            {
                NSData * key = [[NSString stringWithFormat:@"%d", i] dataUsingEncoding:NSUTF8StringEncoding];
                
                NSString * testString = [NSString stringWithFormat:@"test%d", i];
                STAssertEqualObjects(testString, [cache objectForKey:key], @"Cache %@ should have stored these few object.",cacheClassName);
            }
            
            [cache removeAllObjects];
            
            for (NSInteger i = 0; i<capacity; i++)
            {
                NSData * key = [[NSString stringWithFormat:@"%d", i] dataUsingEncoding:NSUTF8StringEncoding];
                
                STAssertNil([cache objectForKey:key], @"Cache %@ should have removed all objects.",cacheClassName);
            }

        }
    }
}

@end

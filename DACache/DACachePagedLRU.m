//
//  DACachePagedLRU.m
//  DACache
//
//  Created by Hendrik Demmer on 1/29/13.
//  Copyright (c) 2013 Hendrik Demmer. All rights reserved.
//

#import "DACachePagedLRU.h"

const NSUInteger kDACachePagedLRUCachePageSize = 2<<12; // 8192 entries on one page

@interface DACachePagedLRU ()
{
    NSUInteger _maxCachePages;
}
@property (nonatomic, strong) NSMutableArray * pages;   // mutable array of mutable dictionaries
@end

@implementation DACachePagedLRU
-(id)initWithCapacity:(NSUInteger)capacity
{
    self = [super init];
    if (self)
    {
        _maxCachePages = ceil(capacity / (float)kDACachePagedLRUCachePageSize);
        self.pages = [NSMutableArray arrayWithCapacity:_maxCachePages];
        [self.pages addObject:[NSMutableDictionary dictionaryWithCapacity:kDACachePagedLRUCachePageSize]];
    }
    return self;
}

- (void)setObject:(id)object forKey:(id<NSCopying>)key
{
    [_pages[0] setObject:object forKey:key];
    if ([_pages[0] count] >= kDACachePagedLRUCachePageSize)
    {
        // the current page is full
        
        if ([_pages count] >= _maxCachePages)
        {
            // max number of pages reached, pop the oldest page into oblivion
            [_pages removeLastObject];
        }
        
        // create a new page
        [_pages insertObject:[NSMutableDictionary dictionaryWithCapacity:kDACachePagedLRUCachePageSize] atIndex:0];
    }
}

- (id)objectForKey:(id)key
{
    for (NSMutableDictionary * cachePage in _pages)
    {
        // search through all pages from newest to oldest
        id object = [cachePage objectForKey:key];
        if (object)
            return object;
    }
    return nil;
}

- (void)removeAllObjects
{
    [_pages removeAllObjects];
    [_pages addObject:[NSMutableDictionary dictionaryWithCapacity:kDACachePagedLRUCachePageSize]];
}

@end

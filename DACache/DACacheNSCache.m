//
//  DACacheNSCache.m
//  DACache
//
//  Created by Hendrik Demmer on 1/29/13.
//  Copyright (c) 2013 Hendrik Demmer. All rights reserved.
//

#import "DACacheNSCache.h"

@interface DACacheNSCache ()
@property (nonatomic, strong) NSCache * cache;
@end

@implementation DACacheNSCache
- (id)initWithCapacity:(NSUInteger)capacity
{
    self = [super init];
    if (self)
    {
        self.cache = [[NSCache alloc] init];
    }
    return self;
}

-(void)setObject:(id)object forKey:(id)key
{
    [self.cache setObject:object forKey:key];
}

-(id)objectForKey:(id)key
{
    return [self.cache objectForKey:key];
}

- (void)removeAllObjects
{
    [self.cache removeAllObjects];
}
@end

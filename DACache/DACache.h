//
//  DACache.h
//  DACache
//
//  Created by Hendrik Demmer on 1/29/13.
//  Copyright (c) 2013 Hendrik Demmer. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DACacheProtocol <NSObject>
- (id) initWithCapacity:(NSUInteger)capacity;
- (id) objectForKey:(id)key;
- (void) setObject:(id)object forKey:(id<NSCopying>)key;
- (void) removeAllObjects;
@end

typedef NSObject<DACacheProtocol> DACache;

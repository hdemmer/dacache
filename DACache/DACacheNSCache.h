//
//  DACacheNSCache.h
//  DACache
//
//  Created by Hendrik Demmer on 1/29/13.
//  Copyright (c) 2013 Hendrik Demmer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DACache.h"

@interface DACacheNSCache : NSObject<DACacheProtocol>

@end

//
//  DACacheMutableDictionary.m
//  DACache
//
//  Created by Hendrik Demmer on 1/29/13.
//  Copyright (c) 2013 Hendrik Demmer. All rights reserved.
//

#import "DACacheMutableDictionary.h"


@interface DACacheMutableDictionary ()
@property (nonatomic, strong) NSMutableDictionary* dictionary;
@end

@implementation DACacheMutableDictionary

- (id)initWithCapacity:(NSUInteger)capacity
{
    self = [super init];
    if (self)
    {
        self.dictionary = [NSMutableDictionary dictionaryWithCapacity:capacity];
    }
    return self;
}

-(void)setObject:(id)object forKey:(id)key
{
    [self.dictionary setObject:object forKey:key];
}

-(id)objectForKey:(id)key
{
    return [self.dictionary objectForKey:key];
}

- (void)removeAllObjects
{
    [self.dictionary removeAllObjects];
}

@end
